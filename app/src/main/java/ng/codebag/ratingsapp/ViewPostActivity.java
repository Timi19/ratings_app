package ng.codebag.ratingsapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

import ng.codebag.ratingsapp.Fragments.ViewAllPostFragment;
import ng.codebag.ratingsapp.Fragments.ViewMyPostFragment;

public class ViewPostActivity extends AppCompatActivity {
     // activity to set up tabs layout to view post
    private String userName,userEmail,userId;

    SharedPreferences prefs;
    String mDefault = "default";
    SharedPreferences.Editor editor;
    FirebaseAuth auth;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_post);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }catch (NullPointerException e){
            e.printStackTrace();
        }

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        setUpViewPager(viewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager,true);

        auth = FirebaseAuth.getInstance();

        prefs = getApplicationContext().getSharedPreferences(getString(R.string.shared_preference_name_one),0);
        userName = prefs.getString(getString(R.string.name),mDefault);
        userEmail = prefs.getString(getString(R.string.email),mDefault);
        userId =  prefs.getString(getString(R.string.id),mDefault);



        Log.d("userNamep", String.valueOf(userId));
        Log.d("userEmailp", String.valueOf(userEmail));
        Log.d("userIdp", String.valueOf(userName));

        FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(ViewPostActivity.this,AddPostActivity.class);
                startActivity(intent1);
            }
        });




    }

    private void setUpViewPager(ViewPager viewPager){
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ViewAllPostFragment(), "Posts");
        adapter.addFragment(new ViewMyPostFragment(), "My Posts");
        viewPager.setAdapter(adapter);

    }

    //inner class to tell which of the fragment to show in each page

    class ViewPagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String>mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager){
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title){
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);

        }

        @Override
        public CharSequence getPageTitle(int position){
            return mFragmentTitleList.get(position);
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_view_postactivity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            editor = prefs.edit();
            auth.signOut();
            editor.putBoolean(getString(R.string.logged_in),false);
            editor.putString(getString(R.string.name),mDefault);
            editor.putString(getString(R.string.email),mDefault);
            editor.putString(getString(R.string.id),mDefault);
            editor.apply();
            startActivity(new Intent(ViewPostActivity.this,LoginActivity.class));
            finish();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

