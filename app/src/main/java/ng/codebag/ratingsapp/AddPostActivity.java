package ng.codebag.ratingsapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.UUID;

import ng.codebag.ratingsapp.Models.Post;
import ng.codebag.ratingsapp.Utils.Constants;

public class AddPostActivity extends AppCompatActivity {

    SharedPreferences prefs;
    String mDefault = "default";
    String userName,userEmail,userId,imageUrl;
    private EditText postEditText;
    private TextInputLayout textInputLayout;
    private DatabaseReference databasePost;
    private Button postButton,picturePostButton;
    ImageView picture,postPictureButton;
    private final int IMG_REQUEST = 1;
    private Bitmap bitmap, mbitmap;
    FirebaseStorage storage = FirebaseStorage.getInstance();
    ProgressBar progressBar;
    private Uri url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_post);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);


        prefs = getApplicationContext().getSharedPreferences(getString(R.string.shared_preference_name_one),0);
        userName = prefs.getString(getString(R.string.name),mDefault);
        userEmail = prefs.getString(getString(R.string.email),mDefault);
        userId =  prefs.getString(getString(R.string.id),mDefault);

        Log.d("userNamePost",userName);
        Log.d("userEmailPost",userEmail);
        Log.d("userIdPost",userId);

        postEditText = (EditText)findViewById(R.id.postEditText);
        postEditText.addTextChangedListener(postTextWatcher);
        postButton = (Button)findViewById(R.id.postButton);
        picture = (ImageView)findViewById(R.id.imageViewPicture);
        postPictureButton = (ImageView)findViewById(R.id.postPictureButton);
        picturePostButton = (Button)findViewById(R.id.postButtonPicture);
        textInputLayout = (TextInputLayout)findViewById(R.id.textInputLayout);
        progressBar = (ProgressBar)findViewById(R.id.postProgressBar);
        progressBar.setVisibility(View.GONE);
        postButton.setEnabled(false);

        databasePost = FirebaseDatabase.getInstance().getReference("posts");

        try {


            postButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final String post = postEditText.getText().toString().trim();

                    String id = databasePost.push().getKey();

                    if (TextUtils.isEmpty(post)) {
                        Toast.makeText(AddPostActivity.this, "post field cannot be empty", Toast.LENGTH_LONG).show();
                        postEditText.setError("this field is required");
                        return;
                    } else {
                        Post mPost = new Post(id, userId, userName, post,Constants.isNotAPicturePost);

                        databasePost.child(id).setValue(mPost);
                        Toast.makeText(AddPostActivity.this, "Post added Successfully", Toast.LENGTH_LONG).show();
                        postEditText.setText(null);
                    }

                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }


        postPictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 selectImage();
            }
        });


        picturePostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                picture.setDrawingCacheEnabled(true);
                picture.buildDrawingCache();
                mbitmap = picture.getDrawingCache();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                mbitmap.compress(Bitmap.CompressFormat.PNG,100,baos);
                picture.setDrawingCacheEnabled(false);
                byte[] data = baos.toByteArray();

                String path = "pictureRatings/" + UUID.randomUUID()+ ".png";
                StorageReference pictureRatingsRef = storage.getReference(path);

                progressBar.setVisibility(View.VISIBLE);
                picturePostButton.setEnabled(false);

                final UploadTask uploadTask = pictureRatingsRef.putBytes(data);

                uploadTask.addOnSuccessListener(AddPostActivity.this, new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        picturePostButton.setEnabled(true);

                        //noinspection VisibleForTests
                         url = taskSnapshot.getDownloadUrl();
                        if (url != null) {
                            imageUrl = url.toString();
                            Toast.makeText(AddPostActivity.this, "image url gotten", Toast.LENGTH_LONG).show();
                        }
                        String id = databasePost.push().getKey();


                        Post mpost = new Post(id,userId,userName,imageUrl, Constants.isAPicturePost);

                        databasePost.child(id).setValue(mpost);
                        progressBar.setVisibility(View.GONE);


                        Toast.makeText(AddPostActivity.this, "Post added Successfully", Toast.LENGTH_LONG).show();

                        textInputLayout.setVisibility(View.VISIBLE);
                        postEditText.setVisibility(View.VISIBLE);
                        postPictureButton.setVisibility(View.VISIBLE);
                        postButton.setVisibility(View.VISIBLE);
                        picture.setVisibility(View.GONE);
                        picturePostButton.setVisibility(View.GONE);



                    }
                });
            }
        });

    }

    TextWatcher postTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            postButton.setEnabled(true);

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    private void selectImage(){
        Intent intent  = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent,IMG_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == IMG_REQUEST && resultCode == RESULT_OK && data != null){
            Uri path = data.getData();

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),path);
                picture.setImageBitmap(bitmap);
                textInputLayout.setVisibility(View.GONE);
                postEditText.setVisibility(View.GONE);
                postButton.setVisibility(View.GONE);
                picture.setVisibility(View.VISIBLE);
                picturePostButton.setVisibility(View.VISIBLE);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    
}
