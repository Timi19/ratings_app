package ng.codebag.ratingsapp.Utils;

/**
 * Created by OmoIyaEmma on 10/18/17.
 */

public class KeepUser {
    private static String user_id;
    private static String userName;
    private static String email;

    public KeepUser() {
    }

    public KeepUser(String user_id, String userName, String email) {
        this.user_id = user_id;
        this.userName = userName;
        this.email = email;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getUserName() {
        return userName;
    }

    public String getEmail() {
        return email;
    }
}
