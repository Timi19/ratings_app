package ng.codebag.ratingsapp.Utils;

import android.util.Patterns;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.regex.Pattern;

/**
 * Created by OmoIyaEmma on 10/17/17.
 */

public class Utils {
    public static String URL_REGEX = "(https://firebase[^\\s]+)";

    public static boolean validEmail(String Email){
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(Email).matches();

    }


    public boolean passwordsMatches(String password1, String password2){
        return password1.equals(password2);
    }

    public static String roundUpDoubleTo2SF(double j){
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);
        return df.format(j);

    }

    public static boolean isAFireBaseStorageUrl(String post){

        return Patterns.WEB_URL.matcher(post).matches();

    }
}
