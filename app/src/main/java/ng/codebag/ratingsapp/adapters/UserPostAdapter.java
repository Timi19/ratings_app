package ng.codebag.ratingsapp.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import ng.codebag.ratingsapp.Models.Post;
import ng.codebag.ratingsapp.Models.PostRatings;
import ng.codebag.ratingsapp.Models.Ratings;
import ng.codebag.ratingsapp.R;
import ng.codebag.ratingsapp.Utils.Constants;
import ng.codebag.ratingsapp.Utils.Utils;

import java.util.List;

/**
 * Created by OmoIyaEmma on 10/18/17.
 */

public class UserPostAdapter extends RecyclerView.Adapter<UserPostAdapter.MyViewHolder> {

    private List<PostRatings> postList;
    private Context mContext;
    ArrayAdapter<CharSequence> arrayAdapter;
    DatabaseReference databaseRatings;
    int [] rating_values = {1,2,3,4,5};


    public UserPostAdapter(List<PostRatings>postList, Context mContext)
    {
        this.postList = postList;
        this.mContext = mContext;


        arrayAdapter =  ArrayAdapter.createFromResource(mContext,R.array.rating_values,android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


    }




    public class MyViewHolder extends RecyclerView.ViewHolder{

        public TextView userNameTextView, postTextView, ratingsTextView;
        public Spinner spinner;
        ImageView pictureView;
        public MyViewHolder(View itemView) {
            super(itemView);

            userNameTextView = itemView.findViewById(R.id.username);
            postTextView = itemView.findViewById(R.id.posts);
            ratingsTextView = itemView.findViewById(R.id.rating);
            spinner=itemView.findViewById(R.id.spinner);
            pictureView = itemView.findViewById(R.id.pictureView);
            databaseRatings = FirebaseDatabase.getInstance().getReference("ratings");
            spinner.setAdapter(arrayAdapter);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View viewItem = layoutInflater.inflate(R.layout.post_card,parent,false);

        return new MyViewHolder(viewItem);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final PostRatings post = postList.get(position);
        holder.userNameTextView.setText(post.getUser_Name());
        //check if post is a picture


        if(post.getIsPicture().equals(Constants.isAPicturePost)){
                Glide.with(mContext).load(post.getPost_body())
                        .placeholder(R.drawable.ic_image_black_24dp).fitCenter().into(holder.pictureView);
                holder.postTextView.setVisibility(View.GONE);
                holder.pictureView.setVisibility(View.VISIBLE);
                Log.d("didthiswor","yep bbro it did");
            holder.setIsRecyclable(false);
            }

        else{
                Glide.clear(holder.pictureView);
                holder.postTextView.setText(post.getPost_body());
                holder.setIsRecyclable(true);
        }



        holder.ratingsTextView.setText(String.valueOf(Utils.roundUpDoubleTo2SF(post.getRatings())));




        holder.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                String ratingS = (String) adapterView.getItemAtPosition(i);
                if(!ratingS.equals("Ratings")) {
                    int rating = Integer.valueOf(ratingS);
                    String post_id = post.getPost_id();
                    String id = databaseRatings.push().getKey();
                    Ratings ratings = new Ratings(id, post_id, rating);
                    databaseRatings.child(id).setValue(ratings);

                    Toast.makeText(mContext, "post rated", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return postList.size();
    }
}
