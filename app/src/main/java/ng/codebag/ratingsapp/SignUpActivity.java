package ng.codebag.ratingsapp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import ng.codebag.ratingsapp.Models.User;
import ng.codebag.ratingsapp.Utils.KeepUser;
import ng.codebag.ratingsapp.Utils.Utils;

public class SignUpActivity extends AppCompatActivity {


    private EditText emailEditText, passwordEditText, repasswordEditText, usernameTextName;
    private Button register;
    private FirebaseAuth auth;
    private DatabaseReference databaseUsers;
    private View mProgressView;
    private View mSignupFormView;
    SharedPreferences prefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //get firebase instance
        auth = FirebaseAuth.getInstance();
        //get database instance and set the node\
        databaseUsers = FirebaseDatabase.getInstance().getReference("users");

        prefs = getApplicationContext().getSharedPreferences(getString(R.string.shared_preference_name_one), Context.MODE_PRIVATE);
        final SharedPreferences.Editor editor = prefs.edit();

        usernameTextName = (EditText) findViewById(R.id.username);
        emailEditText = (EditText) findViewById(R.id.email1);
        passwordEditText = (EditText) findViewById(R.id.password1);
        repasswordEditText = (EditText) findViewById(R.id.password2);
        mSignupFormView = findViewById(R.id.sign_up_form);
        mProgressView = findViewById(R.id.sign_Up_progress);
        register = (Button) findViewById(R.id.sign_in_button);


        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String name = usernameTextName.getText().toString().trim();
                final String email = emailEditText.getText().toString().trim();
                String password = passwordEditText.getText().toString().trim();
                String repassword = repasswordEditText.getText().toString().trim();

                if (TextUtils.isEmpty(name)) {
                    Toast.makeText(getApplicationContext(), " Enter Your Name please", Toast.LENGTH_LONG).show();
                    return;
                }
                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getApplicationContext(), " Enter Email address", Toast.LENGTH_LONG).show();
                    return;
                }
                if(!Utils.validEmail(email)){
                    Toast.makeText(getApplicationContext(),"Please enter a valid password", Toast.LENGTH_LONG).show();
                    return;

                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getApplicationContext(), " Enter Password", Toast.LENGTH_LONG).show();
                    return;
                }

                if (TextUtils.isEmpty(repassword)) {
                    Toast.makeText(getApplicationContext(), " Please Enter Password again", Toast.LENGTH_LONG).show();
                    return;
                }

                if (password.length() < 6) {
                    Toast.makeText(getApplicationContext(), "password lenght must be greater than 6", Toast.LENGTH_LONG).show();
                    return;
                }

                if (!password.equals(repassword)) {
                    Toast.makeText(getApplicationContext(), "passwords not equal", Toast.LENGTH_LONG).show();
                    return;
                } else {
                    showProgress(true);
                    auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(SignUpActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            Toast.makeText(getApplicationContext(), "createUserWithEmail:" + task.isSuccessful(), Toast.LENGTH_LONG);
                            String id = databaseUsers.push().getKey();

                            User user = new User(id, name, email);
                            KeepUser keepUser = new KeepUser(id,name,email);

                            databaseUsers.child(id).setValue(user);
                            Log.d("databaseUsersworking","yes it is");
                            editor.putBoolean(getString(R.string.logged_in), true);
                            editor.putString(getString(R.string.email),email);
                            editor.putString(getString(R.string.name),name);
                            editor.putString(getString(R.string.id),id);
                            editor.commit();

                            if (!task.isSuccessful()) {
                                showProgress(false);
                                Toast.makeText(SignUpActivity.this, "Authentication failed." + task.getException(),
                                        Toast.LENGTH_SHORT).show();
                            } else {
                                showProgress(false);
                                startActivity(new Intent(SignUpActivity.this, ViewPostActivity.class));
                                finish();
                            }
                        }
                    });
                }


            }
        });

    }
    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mSignupFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mSignupFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mSignupFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mSignupFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
