package ng.codebag.ratingsapp.Models;

/**
 * Created by OmoIyaEmma on 10/15/17.
 */

public class Post {

    private String post_id;
    private String user_id;
    private String user_Name;
    private String post_body;
    private String isPicture;


    public Post(){

    }

    public Post(String post_id, String user_id, String user_Name, String post_body, String isPicture) {
        this.post_id = post_id;
        this.user_id = user_id;
        this.user_Name = user_Name;
        this.post_body = post_body;
        this.isPicture = isPicture;
    }

    public String getPost_id() {
        return post_id;
    }

    public String getPost_body() {
        return post_body;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getUser_Name() {
        return user_Name;
    }

    public String getIsPicture() {
        return isPicture;
    }
}
