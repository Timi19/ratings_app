package ng.codebag.ratingsapp.Models;

/**
 * Created by OmoIyaEmma on 10/15/17.
 */

public class Ratings {
    private String ratings_id;
    private String post_id;
    private int ratings;

    public Ratings() {
    }

    public Ratings(String ratings_id, String post_id, int ratings) {

        this.ratings_id = ratings_id;
        this.post_id = post_id;
        this.ratings = ratings;
    }

    public String getRatings_id() {
        return ratings_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public int getRatings() {
        return ratings;
    }
}
