package ng.codebag.ratingsapp.Models;

/**
 * Created by OmoIyaEmma on 10/15/17.
 */

public class User {

    private String user_id;
    private String userName;
    private String email;

    public User(){

    }

    public User(String user_id, String userName, String email) {
        this.user_id = user_id;
        this.userName = userName;
        this.email = email;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getUserName() {
        return userName;
    }

    public String getEmail() {
        return email;
    }
}
