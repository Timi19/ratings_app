package ng.codebag.ratingsapp.Models;

/**
 * Created by OmoIyaEmma on 10/20/17.
 */

public class PostRatings {

    private String post_id;
    private String user_id;
    private String user_Name;
    private String post_body;
    private Double ratings;
    private String isPicture;


    public PostRatings(String post_id, String user_id, String user_Name, String post_body, Double ratings, String isPicture) {
        this.post_id = post_id;
        this.user_id = user_id;
        this.user_Name = user_Name;
        this.post_body = post_body;
        this.ratings = ratings;
        this.isPicture = isPicture;
    }

    public String getPost_id() {
        return post_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getUser_Name() {
        return user_Name;
    }

    public String getPost_body() {
        return post_body;
    }

    public Double getRatings() {
        return ratings;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public void setUser_Name(String user_Name) {
        this.user_Name = user_Name;
    }

    public void setPost_body(String post_body) {
        this.post_body = post_body;
    }

    public void setRatings(Double ratings) {
        this.ratings = ratings;
    }

    public String getIsPicture() {
        return isPicture;
    }
}
