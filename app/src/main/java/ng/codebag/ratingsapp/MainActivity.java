package ng.codebag.ratingsapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    SharedPreferences prefs;
    String mDefault = "mdefault";
    boolean status = false, logged_in;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
     //   setContentView(R.layout.activity_main);

        prefs = getApplicationContext().getSharedPreferences(getString(R.string.logged_in),0);

        logged_in = prefs.getBoolean(getString(R.string.logged_in),status);

        if(logged_in){
            startActivity(new Intent(MainActivity.this,LoginActivity.class));
        }


    }
}
