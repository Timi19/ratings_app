package ng.codebag.ratingsapp.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ng.codebag.ratingsapp.Models.Post;
import ng.codebag.ratingsapp.Models.PostRatings;
import ng.codebag.ratingsapp.Models.Ratings;
import ng.codebag.ratingsapp.R;
import ng.codebag.ratingsapp.Utils.Constants;
import ng.codebag.ratingsapp.Utils.KeepUser;
import ng.codebag.ratingsapp.adapters.UserPostAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewMyPostFragment extends Fragment {

    private UserPostAdapter postAdapter;
    private DatabaseReference databasePosts;
    private DatabaseReference databaseRatings;
    private List<Post> postList = new ArrayList<>();
    private List<Ratings>ratinglist = new ArrayList<>();
    private RecyclerView recyclerView;
    private KeepUser user;
    private Map<String, Integer> totalRatingsValue;
    private Map<String, Integer>totalNoOfRatings;
    PostRatings postRatings;
    private List <PostRatings>postRatingsList = new ArrayList<>();
    private List<Ratings>mRatinglist = new ArrayList<>();
    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressBar progressBar;




    public ViewMyPostFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_view_my_post, container, false);

        databasePosts = FirebaseDatabase.getInstance().getReference("posts");
        databaseRatings = FirebaseDatabase.getInstance().getReference("ratings");
        recyclerView = view.findViewById(R.id.recycler_view1);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefresh1);
        progressBar = view.findViewById(R.id.viewMyPostProgressBar);
        user = new KeepUser();

        totalNoOfRatings = new HashMap<>();
        totalRatingsValue = new HashMap<>();

        getPostFromDatabase();


        postAdapter = new UserPostAdapter(postRatingsList,getActivity().getApplicationContext());
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(postAdapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onRefreshSwiped();

            }
        });

        return view;
    }



    private void getPostFromDatabase(){

        progressBar.setVisibility(View.VISIBLE);

        Log.d("UserIdV",user.getUser_id());
        postList.clear();
        databasePosts.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                try {
                    //ensure that data isnt duplicated as it is called only when a change in data is sensed
                    postList.clear();
                    postRatingsList.clear();
                    ratinglist.clear();
                    totalNoOfRatings.clear();
                    totalRatingsValue.clear();
                    postAdapter.notifyDataSetChanged();
                }catch(NullPointerException e){
                    e.printStackTrace();
                }

                try {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        Post post = postSnapshot.getValue(Post.class);

                        if (user.getUser_id() != null) {

                            if (user.getUser_id().equals(post.getUser_id())) {
                                postList.add(post);
                                totalRatingsValue.put(post.getPost_id(), 0);
                                totalNoOfRatings.put(post.getPost_id(), 0);
                            }

                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                //get all ratings and process the result
                try {
                    databaseRatings.addValueEventListener(new ValueEventListener() {
                        int c = 0;
                        int d = 0;

                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                                Ratings ratings = postSnapshot.getValue(Ratings.class);
                                ratinglist.add(ratings);
                                Log.d("rating" + d, String.valueOf(ratings.getRatings()));
                                d++;
                            }

                            for (Ratings ratingss : ratinglist) {
                                //  need to check if post_id is contained since only posts of the user is included here
                                if (totalRatingsValue.containsKey(ratingss.getPost_id())) {
                                    totalRatingsValue.put(ratingss.getPost_id(), totalRatingsValue.get(ratingss.getPost_id()) + ratingss.getRatings());
                                    totalNoOfRatings.put(ratingss.getPost_id(), totalNoOfRatings.get(ratingss.getPost_id()) + 1);
                                    Log.d("Maps" + c, String.valueOf(totalRatingsValue.get(ratingss.getPost_id()) + ratingss.getRatings()));
                                }


                            }

                            // get the mean
                            int noOfRating = 0;
                            String isApictureHolder = null;

                            for (Post posts : postList) {
                                try {
                                    if(postRatingsList.size() < postList.size()) {
                                        if (totalNoOfRatings.get(posts.getPost_id()) == 0) {
                                            noOfRating = 1;

                                        } else {
                                            noOfRating = totalNoOfRatings.get(posts.getPost_id());
                                        }

                                        if(posts.getIsPicture() == null || posts.getIsPicture().equals(Constants.isNotAPicturePost)){
                                            isApictureHolder = Constants.isNotAPicturePost;
                                        }
                                        else {
                                            isApictureHolder = Constants.isAPicturePost;
                                        }

                                        postRatings = new PostRatings(posts.getPost_id(), posts.getUser_id(), posts.getUser_Name(),
                                                posts.getPost_body(),
                                                ((double) totalRatingsValue.get(posts.getPost_id()) / (double) noOfRating),isApictureHolder);


                                        postRatingsList.add(postRatings);
                                    }

                                    else {
                                        postRatings.setRatings(((double) totalRatingsValue.get(posts.getPost_id()) / (double) noOfRating));
                                    }
                                } catch (ArithmeticException e) {
                                    e.printStackTrace();
                                }
                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }catch (Exception e){
                    e.printStackTrace();
                }
                postAdapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());


            }
        });


    }
    public void onRefreshSwiped(){
        getPostFromDatabase();
        swipeRefreshLayout.setRefreshing(false);
    }

}
