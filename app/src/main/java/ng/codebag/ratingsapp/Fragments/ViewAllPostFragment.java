package ng.codebag.ratingsapp.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ng.codebag.ratingsapp.Models.Post;
import ng.codebag.ratingsapp.Models.PostRatings;
import ng.codebag.ratingsapp.Models.Ratings;
import ng.codebag.ratingsapp.R;
import ng.codebag.ratingsapp.Utils.Constants;
import ng.codebag.ratingsapp.adapters.UserPostAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewAllPostFragment extends Fragment {

    private UserPostAdapter postAdapter;
    private DatabaseReference databasePosts, databaseRatings;
    private List<Post> postList = new ArrayList<>();
    private List<Ratings>ratinglist = new ArrayList<>();
    private List <PostRatings>postRatingsList = new ArrayList<>();
    private RecyclerView recyclerView;
    private Map<String, Integer>totalRatingsValue;
    private Map<String, Integer>totalNoOfRatings;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressBar progressBar;
    PostRatings postRatings;



    public ViewAllPostFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_view_all_post, container, false);

        databasePosts = FirebaseDatabase.getInstance().getReference("posts");
        databaseRatings = FirebaseDatabase.getInstance().getReference("ratings");
        progressBar = view.findViewById(R.id.viewAllPostProgressBar);

        recyclerView = view.findViewById(R.id.recycler_view2);
        swipeRefreshLayout = view.findViewById(R.id.swipeRefresh2);
        getPostFromDatabase();
        postAdapter = new UserPostAdapter(postRatingsList,getActivity().getApplicationContext());
        setUpRecyclerView();


        totalNoOfRatings = new HashMap<>();
        totalRatingsValue = new HashMap<>();

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onRefreshSwiped();
            }
        });


        return  view;
    }

    private void setUpRecyclerView(){
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(postAdapter);
    }





    private void getPostFromDatabase(){

        progressBar.setVisibility(View.VISIBLE);

        databasePosts.addValueEventListener(new ValueEventListener() {



            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    //ensure that data isnt duplicated as it is called only when a change in data is sensed
                    postList.clear();
                    postRatingsList.clear();
                    ratinglist.clear();
                    totalNoOfRatings.clear();
                    totalRatingsValue.clear();
                    postAdapter.notifyDataSetChanged();

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    // get all posts from the db
                    // update maps with post ids
                    Post post = postSnapshot.getValue(Post.class);

                    postList.add(post);
                    totalRatingsValue.put(post.getPost_id(), 0);
                    totalNoOfRatings.put(post.getPost_id(), 0);
                }

                databaseRatings.addValueEventListener(new ValueEventListener() {


                    int c = 0;
                    // new value event listener for ratings
                    int d = 0;
                    int ratingsValue = 0, postValue = 0;

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {


                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                            //get all ratings
                            Ratings ratings = postSnapshot.getValue(Ratings.class);
                            ratinglist.add(ratings);
                            Log.d("ratinggg" + d, String.valueOf(ratings.getRatings()));
                            d++;
                        }

                        try {

                            for (Ratings ratingss : ratinglist) {
                                //get all posts as keys and their ratings as values and the amount of times it occurs
                                if (totalRatingsValue.containsKey(ratingss.getPost_id())) {
                                    totalRatingsValue.put(ratingss.getPost_id(), totalRatingsValue.get(ratingss.getPost_id()) + ratingss.getRatings());
                                    totalNoOfRatings.put(ratingss.getPost_id(), totalNoOfRatings.get(ratingss.getPost_id()) + 1);
                                    Log.d("Mapped" + c, String.valueOf(totalNoOfRatings.get(ratingss.getPost_id())));
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                        int noOfRating = 0;
                        String isApictureHolder = null;

                        for (Post posts : postList) {

                            try {
                                if (postRatingsList.size() < postList.size()) {
                                    if (totalNoOfRatings.get(posts.getPost_id()) == 0) {
                                        noOfRating = 1;

                                    } else {
                                        noOfRating = totalNoOfRatings.get(posts.getPost_id());
                                    }
                                    if(posts.getIsPicture() == null || posts.getIsPicture().equals(Constants.isNotAPicturePost)){
                                        isApictureHolder = Constants.isNotAPicturePost;
                                    }
                                    else {
                                        isApictureHolder = Constants.isAPicturePost;
                                    }
                                    // generate new postrating object and add to the list
                                    postRatings = new PostRatings(posts.getPost_id(), posts.getUser_id(), posts.getUser_Name(),
                                            posts.getPost_body(),
                                            ((double) totalRatingsValue.get(posts.getPost_id()) / (double) noOfRating),isApictureHolder);

                                    postRatingsList.add(postRatings);
                                    Log.d("finalstrings", String.valueOf(totalRatingsValue.get(posts.getPost_id()) / noOfRating));
                                }

                                else {
                                    postRatings.setRatings(((double) totalRatingsValue.get(posts.getPost_id()) / (double) noOfRating));
                                }
                            } catch (ArithmeticException | NullPointerException e) {
                                e.printStackTrace();
                            }

                        }
                    }


                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
                postAdapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);

            }



            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());


            }
        });


    }

    public void onRefreshSwiped(){
        getPostFromDatabase();
        swipeRefreshLayout.setRefreshing(false);
    }

}
